/// this is a fahrenheit to celsius and vice versa converter
///

use std::io;

fn main() {

    println!("Welcome to Scot's Temperature converter! Press 1 for F to C, 2 for C to F, or 0 to quit");

    let mut option = String::new();
    io:: stdin().read_line(&mut option);
   

    
    while option != 0 {

    let option: u32 = match option.trim().parse() {
        Ok(num) => num,
    Err(_) => continue,};
       if option == 0 {
           println!("Bye!");
           } 
       else {

        let mut degrees = String:: new();
         io:: stdin().read_line(&mut degrees)
            .expect("Please enter a number");

        let degrees: f64 = match degrees.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        if option == 1 {
            println!("Enter # of degrees to be converted");
            let answer = (degrees -32.0) / 1.8;
            println!("That converts to {:.2} degrees Celsius.", answer);

        } else {
            println!("Enter # of degrees to be converted");
            let answer = degrees * 1.8 + 32.0;
            println!("That converts to {:.2} degrees Fahrenheit.", answer);
           }
        }
    }
}



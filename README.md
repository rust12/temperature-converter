This is a temperature converter application for a linux terminal.
The goal is to provide a temperature converter from Celsius to
Fahrenheit and vice versa.
Known issues:
* you have to enter 0 twice to escape
the application.